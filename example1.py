#-----------------------------------------------------------------------
# EM on 08/06/2021
#Example on how PCA works
#Data includes 4 features of 3 classes of flowers
#Here, only two features of 'petal length' and 'petal width' are selected
#The outcome is compared with PCA library of sklearn in python
#-----------------------------------------------------------------------
import module_pcaclass as mpca
import module_plotclass as mplot
import module_color as mclr
import matplotlib.pyplot as plt
import pandas as pd
#----------------------------------------------------------------------
source='./iris.data'
#Data information
#Read data
data= pd.read_csv(source,quotechar=",",names=\
                   ['Sepal length','Sepal width',\
                    'Petal length','Petal width','Species'],\
                   header=None)

#Flower types
target=['Iris-setosa','Iris-versicolor','Iris-virginica']
#Two features of flower
features=['Petal length','Petal width']
#Two selected features in calculations
selected_features=['Petal length','Petal width']
#Title for type column of flowers
indx_title='Species'
#Used in sklearn library functions
#number of components
n_comp=2
#Name of principal components whose number should be the same as n_comp
column=['PC1','PC2']
#For plot purposes
#For legend colour related to each 3 kind of flowers in data
colors=['r','g','b']
size_fig=(10,8)
nrow=2 
ncol=2
#-----------------------------------------------------------------------
mthd=mpca.pca_mthd(features,indx_title,data)
#PCA by mathematica methods
pca_alg=mthd.algrtm(selected_features,target)
#-----------------------------------------------------------------------
#PCA by python library
pca_lib,var_cuml,data_combns=mthd.pyth_lib(n_comp,column)
#-----------------------------------------------------------------------
print(mclr.bcolors.OKBLUE+'Takes only two features of petal length and width')
print('and plot two principal components obtained by')
print('mathematical method and sklearn library')
print('-------------------------------------------------'+mclr.bcolors.ENDC)
#Plot figures
pl=mplot.plotgen()
fig, ((ax1, ax2),(ax3,ax4)) =\
    plt.subplots(figsize=size_fig,nrows=nrow, ncols=ncol)
pl.figplot(ax1,selected_features[0],\
           selected_features[0],\
           selected_features[1],\
           selected_features[1],\
           'Petal size distribution of 3 flower species',\
           target,colors,data,indx_title)

fig.delaxes(ax2)

pl.figplot(ax3,'PC1',\
           selected_features[0],\
           'PC2',\
           selected_features[1],\
           'PCA by math. method',\
           target,colors,pca_alg,indx_title)
ax3.set_ylim(-1.,1.2)

pl.figplot(ax4,column[0],\
           column[0],\
           column[1],\
           column[1],\
           'PCA by python library',\
           target,colors,pca_lib,indx_title)
ax4.set_ylim(-1.,1.2)

plt.tight_layout()
plt.show()

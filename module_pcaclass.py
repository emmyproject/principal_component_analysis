import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import scipy.linalg as la
import statistics
#Standardizes datasets to a unit scale (mean=0 and variance=1)
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
#---------------------------------------------------------------
class pca_mthd():   
    
    def __init__(self,features,indx,dat):
        self.features=features
        self.indx=indx
        self.dat=dat

    def algrtm(self,select_feature,targets):
        """
        By mathematical technique:
        1- Form m (sample) by n (feature) matrix X where 
        2- Center the data by x_i --> x_i-1/m \sum_i=1^m x_i 
        3- Form the n by n covariance matrix by C=X^T.X=sum_i=1^m x_i.x_i^T
        4- Diagonalize C = X^T.X = W Λ W^T, where W are the normalised eigenvectors,
           or PCA, and Λ is a diagonal matrix containing the eigenvalues.
           In general, pick the largest eigenvalues and the corresponding eigenvectors 
        """
        final = pd.DataFrame(columns=select_feature)
        #Step 1
        #separating features
        data=self.dat[select_feature]
        #Step 2
        data_cent=data-data.mean()
        #standardiazation
        std=self.std_dev(data,select_feature)
        data_stdz=data_cent/std
        #Step 3
        data_tpos=np.transpose(data_stdz) 
        cov_mat=np.dot(data_tpos,data_stdz)/(len(data)-1) 
        #Step 4
        eig_val,eig_vec=la.eig(cov_mat)           
        #sort eigenvalues from max value to min value 
        #and also corresponding eigenvectors 
        #NOTE:
        #eigenvalues are the same as variance value
        idx=eig_val.argsort()[::-1] 
        eig_val=eig_val[idx]
        eig_vec=eig_vec[:,idx]
        #Extra check of eigenvectors
        for i in range(len(select_feature)):
            if np.all(eig_vec[:,i]==-abs(eig_vec[:,i])):
                eig_vec[:,i]=-eig_vec[:,i]
            else:
                eig_vec[:,i]=eig_vec[:,i]
        #make principal components
        data_trsfm=np.dot(data_stdz,eig_vec) 
        finalDF=pd.DataFrame(data_trsfm,columns=select_feature)
        """
        Calculate variance and fractional variance of each feature 
        """       
        var_math,varfrac_math=self.var_alg(finalDF,select_feature)
        #print('Fractional variance of each feature by math calculation is:')
        #self.print_out(varfrac_math)
        
        return pd.concat([finalDF,self.dat[[self.indx]]], axis=1)

    
    def pyth_lib(self,nc,column):
        """
        PCA using python library sklearn
        """
        #separating species
        y=self.dat.loc[:,[self.indx]].values
        #separating features
        x=self.dat.loc[:,self.features].values
        #standardizing the features into unit scale
        x=StandardScaler().fit_transform(x)
        #x=self.dat[self.features]-self.dat[self.features].mean()  
        #selecting number of principal components
        pca=PCA(n_components=nc)
        #Fit in transform and get a 2 dimensional data
        principalComponent=pca.fit_transform(x)
        DF=pd.DataFrame(principalComponent, \
                                 columns=column)  
        finalDF=pd.concat([DF,self.dat[[self.indx]]],axis=1)
        #pca.components_ calculate eigenvectors
        explvar=pca.explained_variance_#np.insert(pca.explained_variance_,0,0)
        frac_var=np.round(pca.explained_variance_ratio_,decimals=3)#np.insert(pca.explained_variance_ratio_,0,0)
        cumlvar=np.cumsum(np.round(frac_var,decimals=3)) 
        DF_pc=pd.DataFrame(column,columns=['PCs'])  
        
        DF_var=pd.DataFrame(frac_var,columns=['Fractional variance'])
        DF_cumlvar=pd.DataFrame(cumlvar,columns=['Cumulative variance'])
        DF_var_cuml=pd.concat([DF_pc,DF_var,DF_cumlvar],axis=1)
        #calculates the linear combination of features
        #for each principal components
        loadings=np.round(pca.components_.T,decimals=3)
        DF_loadings=pd.DataFrame(loadings,columns=column,index=self.features)
        #print('Fractional variance of each feature by sklearn library is:')
        #self.print_out(frac_var)
        #Final data frame which includes species column along axis=1
        return finalDF,DF_var_cuml,DF_loadings
    
    def var_alg(self,data,column):
        target_data=data[column]
        """
        Unbiased sample variance is var=sum(x_i-x_mean_i)^2/(n-1)
        """
        var=np.sum((target_data-target_data.mean())**2)/(len(target_data))
        frac_var=var/np.sum(var)
        return var,frac_var
    
    def std_dev(self,data,column):
        var=self.var_alg(data,column)[0]
        return np.sqrt(var)
        
    
    def print_out(self,varble):
        for i in range(len(varble)):
            print('PC({0}): {1:1.3f}'.format(i+1,varble[i]))
        print('  ')

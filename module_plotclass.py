import matplotlib.pyplot as plt
#------------------------------------
class plotgen():
    
    def figplot(self,ax,xlab,feat1,ylab,feat2,tit,targets,colors,dat,spec):
        ax.set_xlabel(xlab,fontsize=15)
        ax.set_ylabel(ylab,fontsize=15)
        ax.set_title(tit,fontsize=10)
        for spc,clr in zip(targets,colors):
            indicesToKeep=dat[spec]==spc
            ax.scatter(dat.loc[indicesToKeep,feat1],\
                       dat.loc[indicesToKeep,feat2],\
                       c=clr,s=50)
        ax.legend(targets,fontsize=15)
        ax.grid()    

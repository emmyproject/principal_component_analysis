#-------------------------------------------------------------------------
# EM on 08/06/2021
#Example of choosing different principal components by sklearn library
#Data gives 4 features of 3 types of flowers
#Depending on the desired number of principal components,
#n_comp and coulmn list are changed
#-------------------------------------------------------------------------
import module_color as mclr
import module_pcaclass as mpca
import matplotlib.pyplot as plt
import pandas as pd
#------------------------------------------------------------------------
#Data information
source='./iris.data'
#Read data
data= pd.read_csv(source,quotechar=",",names=\
                   ['Sepal length','Sepal width',\
                    'Petal length','Petal width','Species'],\
                   header=None)
#Flower type
target=['Iris-setosa','Iris-versicolor','Iris-virginica']
#Given features of flowers
features=['Sepal length','Sepal width','Petal length','Petal width']
#Title for type column of flowers
indx_title='Species'

#Used in sklearn library functions
#number of components
n_comp=4
#Name of principal components whose number should be the same as n_comp
column=['PC1','PC2','PC3','PC4']
#-----------------------------------------------------------------------
#PCA by python library
exmpl=mpca.pca_mthd(features,indx_title,data)
pca_lib,var_cuml,data_combns=exmpl.pyth_lib(n_comp,column)
print('')
print(mclr.bcolors.BOLD+' PCA of a dataset of 3 species of flowers')
print('           with 4 features'+mclr.bcolors.ENDC)
print('===========================================================')
print(mclr.bcolors.BLUE+'Combinations of data features against selected PCs:'\
      +mclr.bcolors.ENDC)
print('------------------------------------------------')
print(data_combns)
print('')
print(mclr.bcolors.GREEN+'Fractional and cumulative variance for each PCs:'\
      +mclr.bcolors.ENDC)
print('------------------------------------------------')
print(var_cuml)
print('')
#Plot figure
fig, (ax1, ax2) = plt.subplots(figsize=(10,5),nrows=1, ncols=2)
ax1.plot(var_cuml['PCs'],var_cuml['Cumulative variance'],marker='o',
         color='g'
         )
ax1.set_xlabel('Principal components')
ax1.set_ylabel('Cumulative variance')
ax1.set_ylim(0,1)

ax2.bar(var_cuml['PCs'],var_cuml['Fractional variance'],
        label=var_cuml['Fractional variance'],
        color='g'
       ) 

ax2.set_xlabel('Principal components')
ax2.set_ylabel('Fractional variance')
        
plt.tight_layout()
plt.show()

Shows how principal component analysis works
------------------------------------------------------------------------
There are two examples:

1- example1.py is a code based on mathematical concepts
   Data includes 4 features of 3 classes of flowers
   where only two features of 'petal length' and 'petal width' are selected
   The outcome is compared with PCA library of sklearn in python

2- example2.py is an example of choosing different principal components
   by sklearn library.
   Data gives 4 features of 3 types of flowers
   Depending on the desired number of principal components,
   n_comp and coulmn list are changed


The jupyter notebook file PCA.ipynb also exists 


